'use strict';
$(function() {
  $.getScript('js/compiled.js', function() {
    getCompiledScripts(function() {
      var paginationData = null;
      // Индексы помеченных галочками задач
      var checkedParams = [];
      var taskModified = {
        taskContent: null,
        index: null
      };
      /**
       * Отображает задачи на экране
       * @param pageLength Количество задач на странице
       * @param currentPage Текущая страница
       */
      function renderContent(pageLength, currentPage) {
        if (arguments.length) // если значения не дефолтные
          paginationData = {
            pageLength: pageLength,
            currentPage: currentPage,
            original: false
          };
        else {
          paginationData = {
            pageLength: 20,
            currentPage: 1,
            original: true
          };
        }
        checkedParams = [];
        $.get('/index', paginationData).done(function(result) {
          // Вот пока не извратиться (с анонимными функциями) - не заработает, блин.
          return function(p) {
            // Получили подмножество результата и отображаем его на странице
            $(' #tasks').html(displayTasks({
              tasks: result.tasks
            }));
            $(' #pagination').html(displayPagination({
              currentPage: p.currentPage,
              pageCount: result.pageCount
            }));
          }(paginationData);
        });
      }
      // Отобразим 1-й раз с дефолтными значениями
      renderContent();

      // Новая задача
      $(' #newTask').click(function() {
        var dataAdded = $(' #dataAdded');
        if (!dataAdded.val()) {
          displayAlert('Невозможно добавить задачу, так как она пуста', 'alert-danger');
          return;
        }
        taskModified.taskContent = dataAdded.val();
        dataAdded.val('');
        taskModified.index = null;
        $.post('/addTask', taskModified).done(function(result) {
          taskModified.taskContent = taskModified.index = null;
          renderContent(paginationData.pageLength, result['lastPage']);
          displayAlert('Задача успешно добавлена', 'alert-success');
        });
      });
      $(' .toggle-display').on('click', function() {
        renderContent($(this).data('pagelength'), 1);
      });
      var body = $(' body');
      // Навигация
      body
        .on('click', '.switch-page', function() {
          renderContent(paginationData.pageLength, $(this).data('index'));
        })
        .on('click', '.switch-page-previous', function() {
          renderContent(paginationData.pageLength, paginationData.currentPage - 1);
        })
        .on('click', '.switch-page-next', function() {
          renderContent(paginationData.pageLength, paginationData.currentPage + 1);
        });
      // Все, связанное с задачами
      body
        .on('click', '.edit', function() {
          // Редактирование задачи
          var input = $(this).data('input');
          taskModified.taskContent = $('#'+input).val();
          taskModified.index = $(this).data('index');
          $.post('/index', taskModified).done(function() {
            taskModified.taskContent = taskModified.index = null;
            displayAlert('Задача успешно отредактирована', 'alert-success');
          });
        })
        .on('click', '.selectTask', function() {
          var input = $(this).data('input'), index = $(this).data('index');
          if (this.checked) {
            checkedParams.push(index);
          }
          else {
            checkedParams.splice(checkedParams.indexOf(index), 1);
          }
        })
        .on('click', '.share', function() {
          var input = $(this).data('input');
          $.get('/share', $('#' + input).val()).done(function(result) {
            $(' #modal').html(displayFriends({
              users: result
            }));
            $(' #modal-share').modal({
              keyboard: true
            }).show();
          });
        });

      // переключить все галочки
      $(' #toggleSelection').on('change', function() {
        $(' .selectTask').click();
      });

      $(' #deleteSelected').on('click', function() {
        $.post('deleteTasks', {
          tasks: JSON.stringify(checkedParams)
        }).done(function() {
          displayAlert('Удаление выполнено успешно. Количество удаленных задач: ' + checkedParams.length, 'alert-success');
          renderContent(paginationData.pageLength, 1);
          checkedParams = [];
        });
      });
    });
  });
});