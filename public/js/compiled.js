/**
 * Делаем запрос на /files и тянем все пришедшие скрипты
 * (всю папку js/bin + js/runtime.js)
 * @param callback Функция, которая будет вызвана после обработки последнего скрипта
 */
$.getCompiledScripts = function(callback) {
  $.ajax({
    type: 'POST',
    url: '/files',
    settings: {
      // Изначально этот параметр служил защитой против юзеров,
      // которые бы полезли на /files (если нет такого - вернуть 403).
      // Защиту убрал, ибо не заработало. А параметр остался, бог с ним.
      gettingFiles: true,
      async: false
    }
  }).done(function(scripts) {
    async.map(scripts, function(script, _callback) {
      $.getScript(script, function() {
        _callback(null, script);
      })
    }, function() {
      callback(scripts);
    });
  });
};