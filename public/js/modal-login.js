$(function() {
  "use strict";
  // переключим атрибут type с text на password и обратно
  var password = $(' #password');
  $(' #toggle-password-shown').on('click', function() {
    var currentType = password.attr('type');
    var newType = (currentType === 'password') ? 'text' : 'password';
    password.attr('type', newType);
  });
  $(' #btn-save').on('click', function() {
    password.attr('type', 'text'); // на всякий случай
    $.post('/welcome', $(' #form-login').serialize()).done(function(result) {
      if (result.loginError) {
        displayAlert('Неправильный логин или пароль', 'alert-danger');
      }
      else if (result.url) {
        $(' #url-destination').val(result.url);
        $(' #form-success').submit();
      }
    });
  });
});