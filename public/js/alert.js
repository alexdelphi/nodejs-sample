var displayAlert = function(obj) {
  if (arguments.length == 2) {
    obj = {
      Text: arguments[0],
      Class: arguments[1]
    };
  }
  if (!obj.Text) {
    obj.Text = 'default alert';
  }
  if (!obj.Class) {
    obj.Class = 'alert-primary';
  }
  $('#alert-display').addClass(obj.Class).show();
  $('#alert-text').html(obj.Text);
  if (!this.clickHandlerSet)
  {
    $('#btn-close-alert')
      .on('click', function() {
        var alert = $('#alert-display');
        var originalClass = alert.attr('original-class');
        alert.attr('class', originalClass).attr('style', 'display: none');
      });
  }
  this.clickHandlerSet = true;
};