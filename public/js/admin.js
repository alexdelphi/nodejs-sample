'use strict';
$(function() {
  $.getScript('js/compiled.js', function() {
    getCompiledScripts(function(scripts) {
      var userNames = {};
      function renderUsers() {
        $(' #users').html("");
        $.post('/admin/users', {}).done(function(users) {
          $(' #users').html(displayUsers({
            users: users.map(function(user) {
              return user['login'];
            })
          }));
        });
      }
      $(' body')
        .on('click', '#toggleSelection', function() {
          // переключить галочки
          //window.console.log(JSON.stringify(userNames));
          $(' .selectUser').click();
        })
        .on('click', '#deleteSelected', function() {
          // удалить помеченные
          $.post('/admin/users/delete', userNames).done(function(result) {
            var alertClass = '', deletedSelfWarning = '';
            if (result.triedDeleteSelf) {
              alertClass = 'alert-warning';
              deletedSelfWarning = '<br>Владелец не может удалить сам себя';
            }
            else {
              alertClass = 'alert-success';
            }
            displayAlert('Удалено пользователей: '
              + result.deletedUsersCount
              + deletedSelfWarning,
              alertClass);
            userNames = {};
            renderUsers();
          });
        })
        .on('click', '.selectUser', function() {
          var input = $(this).data('input');
          var index = $(this).data('index');
          if (this.checked) {
            userNames['' + index] = $('#' + input).val();
          }
          else {
            delete userNames[index];
          }
        });
      renderUsers();
      $(' #btn-compile').on('click', function() {
        $.get('/admin/compile', {}).done(function() {
          displayAlert('Все файлы успешно перекомпилированы.<br>Рекомендуется перезапустить сервер', 'alert-success');
        });
      });
    });
  });
});