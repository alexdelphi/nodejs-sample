$(function() {
  $.getScript('js/compiled.js', function() {
    getCompiledScripts(function() {
      $(' #btn-submit').on('click', function() {
        $.post('/register', {
          login:    $(' #login').val(),
          password: $(' #pass').val(),
          email:    $(' #email').val()
        }).done(function(result) {
          if (result.userAlreadyExists) {
            displayAlert('Данный пользователь уже существует', 'alert-danger');
          }
          else if (result.url) {
            window.location.href = result.url;
          }
        })
      })
    })
  })
});