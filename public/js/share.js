// Обработчики модального окна
$(function() {
  var users = {};
  var body = $(' body');
  body.on('click', '.selectUser', function() {
    var input = $(this).data('input'), index = $(this).data('index');
    if (this.checked) {
      users['' + index] = $('#' + input).val();
    }
    else {
      delete users[index];
    }
  }).on('click', '#shareSelected', function() {
    $.post('/share', users).done(function() {
      $(' #modal-share').hide();
      $(' #modal').html('');
      displayAlert('Задача выложена в общий доступ. Количество пользователей: ' + Object.keys(users).length, 'alert-success');
      users = {};
      $(' .modal-backdrop').remove();
      $(' body').removeAttr('class').removeAttr('style');
    });
  }).on('click', '#selectAll', function() {
    $(' .selectUser').click();
  });
});