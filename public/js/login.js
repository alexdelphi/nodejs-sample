$(function() {
  "use strict";
  $(' #btn-login').on('click', function() {
    $.post('/loginTemp', {
      login: $(' #login').val(),
      password: $(' #password').val()
    }).done(function(result) {
      if (result.loginError) {
        displayAlert('Неправильный логин или пароль', 'alert-danger');
      }
    })
  });
});