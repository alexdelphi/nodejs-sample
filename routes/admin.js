'use strict';
var express       = require('express');
var router        = express.Router();
var schemas       = require('server/schemas');
var defaults      = require('server/defaults');
var utils         = require('server/utils');
var nodemailer    = require('nodemailer');

router.get('/', function(req, res) {
  if (!req.session['userId']) {
    // А посылку я вам не отдам. Потому что у вас докУментов нету.
    res.status(403);
    res.render('errors/error403', {
      url: req.protocol + '://' + req.get('host') + req.originalUrl
    });
    return;
  }
  utils.findUser(req.session['userId'], function(user) {
    res.render('admin', {
      title: 'Админка',
      currentUser: user
    });
  });
});
router.post('/', function(req, res) {
});

router.get('/compile', function(req, res) {
  var fs = require('fs');
  var path = require('path');
  var jade = require('pug');
  var pathSrc = 'views/src/';
  var pathDest = 'js/bin/';
  var fullPath = path.join(__dirname, '../', pathSrc);
  fs.readdir(fullPath, function(err, files) {
    if (err) {
      throw err;
    }
    files.forEach(function(file) {
      var baseName = file.split('.')[0]; // имя файла без расширения
      var displayName = baseName.substr(1, baseName.length - 1);
      var fnName = 'display' + baseName[0].toUpperCase() + displayName;
      var fn = jade.compileFileClient(path.join(fullPath, file), {
        name: fnName,
        compileDebug: true
      });
      fs.writeFileSync(path.join(__dirname, '../public', pathDest + baseName + '.js'), fn);
    });
  });
  res.send({});
});

router.post('/users', function(req, res) {
  utils.findAllUsers(function(users) {
    res.json(users);
  });
});

// Ищет и удаляет пользователей. Владелец не может удалить сам себя
router.post('/users/delete', function(req, res) {
  utils.findUser(req.session['userId'], function(user) {
    var array = [], userIndex = -1;
    for (var item in req.body) {
      if (req.body[+item] === user['login']) {
        userIndex = +item;
      }
      else {
        array.push(+item);
      }
    }
    utils.deleteUsers(array, function(result) {
      res.json({
        remainingUsers: result,
        triedDeleteSelf: (userIndex != -1),
        deletedUsersCount: array.length
      });
    });
  });
});

module.exports = router;