'use strict';
var express = require('express');
var schemas = require('server/schemas');
var router  = express.Router();
var util    = require('util');

router.get('/', function(req, res) {
  res.render('register', { title: 'Регистрация'});
});
router.post('/', function(req, res) {
  console.log(req.body);
  // Есть такой юзер?
  schemas.User.find({
    $or: [
      {login: req.body['login']},
      {email: req.body['email']}
    ]
  }).exec(function (err, result) {
    if (result.length == 0) {
      new schemas.User({
        login: req.body['login'],
        password: req.body['password'],
        email: req.body['email'],
        privileges: 'user' // стандартный пользователь
      }).save(function (err, user) {
          if (err) {
            throw(err);
          }
          req.session['userId'] = user._id;
          req.session.save();
          res.json({
            url: '/index'
          });
        });
    }
    else {
      res.send({
        userAlreadyExists: true
      });
      console.log("user exists");
    }
  })
});

module.exports = router;
