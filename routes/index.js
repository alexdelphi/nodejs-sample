'use strict';
var express       = require('express');
var router        = express.Router();
var schemas       = require('server/schemas');
var defaults      = require('server/defaults');
var utils         = require('server/utils');
var util          = require('util');

router.get('/', function(req, res) {
  // Если не заданы настройки пользователя - получим их для начала
  if (!req.session['userSettings']) {
    req.session['userSettings'] = defaults;
    req.session.save();
  }
  if (req.query.original) {
    // Если не 1-я отрисовка
    req.session['userSettings']['paginationData'] = req.query;
    req.session.save();
  }
  var pages = req.session['userSettings']['paginationData'];
  utils.findTasks(req.session['userId'], pages, function(_tasks, user, pageCount) {
    var params = {
      title: 'Задачи | Hello World!',
      currentUser: user,
      pageCount: pageCount,
      tasks: _tasks.map(function (task) {
        return task.name;
      }),
      paginationData: pages
    };
    if (!req.query.original) {
      // Если 1-я отрисовка
      res.render('index', params);
    }
    else {
      res.json(params);
    }
  });
});
router.post('/', function(req, res) {
  // обновляем контент
  schemas.Task.find()
    .populate('user')
    .find({
      user: req.session['userId']
    })
    .skip(req.body['index'])
    .findOne(function(err, task) {
      task.name = +req.body['taskContent'];
      task.save(function (err) {
        if (err) {
          throw err;
        }
        req.session.save();
        res.redirect('/index');
      })
    })
});

module.exports = router;