'use strict';
var express       = require('express');
var router        = express.Router();
var utils         = require('server/utils');

 /*
  * Получаем список юзеров, которым можно расшарить задачу
  */
router.get('/', function(req, res) {
  utils.findAccessibleUsers(req.session['userId'], function(users) {
    res.json(users.map(function(user) {
      return user.login;
    }));
  });
});

 /*
  * Добавляем новую задачу.
  */
router.post('/', function(req, res) {
  for (var item in req.body) {
    console.log(req.body[+item]);
  }
  res.json({});
});

module.exports = router;