'use strict';
var express = require('express');
var router  = express.Router();
var schemas = require('../server/schemas');
var util    = require('util');

router.get('/', function(req, res) {
  res.render('welcome', {title: 'Hello World!'});
});

router.post('/', function(req, res) {
  // юзер залогинился, ищем имя/пароль
  schemas.User.find({
    $and: [
      {login: req.body['login']},
      {password: req.body['password']}
    ]
  }).exec(function(err, users) {
    if (err || (users.length == 0)) {
      // А ты кто?
      res.json({
        loginError: true
      });
    }
    else {
      // Нашли юзера - редиректим
      req.session['userId'] = users[0]._id;
      req.session.save();
      var redirectUrl = (users[0]['privileges'] === 'admin') ? '/admin' : '/index';
      res.json({
        url: redirectUrl
      });
    }
  });
});

router.post('/submit', function(req, res) {
  res.redirect(req.body['url']);
});

module.exports = router;
