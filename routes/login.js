'use strict';
var express = require('express');
var router  = express.Router();
var schemas = require('../server/schemas');
var util    = require('util');

router.get('/', function(req, res) {
  if (req.session['userId']) {
    schemas.User.find({
      userId: req.session['userId']
    }).exec(function(err) {
      if (err) {
        throw err;
      }
      res.redirect('/index');
    });
  }
  else {
    res.render('login', {title: 'Войти'});
  }
});

module.exports = router;
