'use strict';
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('config/index');
const log = require('server/log')(module);
const session = require('express-session');
const jade = require('pug');
const schemas = require('server/schemas');
const utils = require('server/utils');
const app = express();
require('dotenv').config();

/* TODO Расшаривать задачи другому пользователю. Не обязательно динамическое изменение */

/*
 * TODO
 * 1) Модалку с профилем украсить при помощи CSS
 * 2) Собственно задачи ($.post('/share') из модального окна).
 * 3) Изоляция админки от остальных юзеров и в админке - возможность сделать админом
 * 4) Подумать, как можно сделать листы расшаренных задач
 *    (фиктивное поле владельца). Туда же пихнуть удаление.
 * 5) Если расшаренную задачу удаляет владелец - сделать предупреждение хотя бы
 */
const _session = session({
    secret: 'some Session',
    resave: false,
    saveUninitialized: true,
    cookie: {secure: false}
});

app.listen(config.get('port'), function () {
    log.info('Listening on port ' + config.get('port'));
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(_session);
app.use(function (req, res, next) {
    req.db = schemas.db;
    next();
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/loginTemp', require('./routes/login'));
app.use('/index', require('./routes/index'));
app.use('/welcome', require('./routes/welcome'));
app.use('/register', require('./routes/register'));
app.use('/admin', require('./routes/admin'));
app.use('/share', require('./routes/share'));

/* контроллеры */
app.get('/', function (req, res) {
    res.redirect(req.session['userId'] ? '/index' : '/welcome');
});
// Отдаем файлы, которые требуется получить динамически
app.post('/files', function (req, res) {
    var compiledPaths = require('server/defaults').compiledPaths;
    var pathToSearch = compiledPaths.pathToSearch;
    var fullPath = __dirname + '/public/' + pathToSearch;
    var fs = require('fs');
    fs.readdir(fullPath, function (err, files) {
        if (err) {
            throw err;
        }
        var result = [compiledPaths.runtime];
        result = result.concat(files.map(function (file) {
            return path.join(pathToSearch, file);
        }));
        res.json(result);
    });
});

app.post('/addTask', function (req, res) {
    var textToAdd = req.body['taskContent'];
    var userId = req.session['userId'];
    new schemas.Task({
        name: textToAdd,
        user: userId
    }).save(function (err) {
        if (err) {
            throw(err);
        }
    });
    utils.findTasks(req.session['userId'], req.session['userSettings']['paginationData'], function (tasks, user, pageCount) {
        res.json({
            lastPage: pageCount
        });
    });
});
app.post('/deleteTasks', function (req, res) {
    var tasksToDelete = JSON.parse(req.body['tasks']);
    // переделать с учетом листов
    utils.findTasks(req.session['userId'], req.session['userSettings']['paginationData'], function (tasks) {
        tasks = tasks.removeIndices(tasksToDelete, function (val, i) {
            val.remove();
        });
        var names = tasks.map(function (item) {
            return item.name;
        });
        res.send(names);
    });
});
app.route('/admin')
    .get(function (req, res) {

    });

app.route('/clearTables')
    .get(function (req, res) {

    });

app.route('/logout')
    .get(function (req, res) {
        req.session.destroy();
        res.redirect('/welcome');
    });

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: app.get('env') === 'development' ? err : {}
    });
});

module.exports = app;