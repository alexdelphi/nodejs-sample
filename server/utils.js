/**
 * Работа с БД.
 * @module server/utils
 */
'use strict';
var schemas = require('./schemas');
var mongoose = require('mongoose');
var util = require('util');
var async = require('async');
/**
 * @template T
 * @typedef function(T, Number) ElementAction
 * @property {T} value Элемент
 * @property {Number} index Индекс
 */
/**
 * Удаление из массива элементов с заданными индексами
 * @template T
 * @param {Number[]} removedArrayIndices Индексы элементов; должны идти подряд
 * @param {ElementAction=} action Действие над элементом массива
 * @returns {T[]} "Очищенный" массив
 */
Array.prototype.removeIndices = function (removedArrayIndices, action) {
    removedArrayIndices.sort(function (a, b) {
        return a - b;
    });
    var j = 0;
    var newArray = [];
    for (var i = 0; i != this.length; i++) {
        if (removedArrayIndices[j] === i) {
            if (arguments.length == 2) {
                action(this[i], i);
            }
            j++;
        }
        else {
            newArray.push(this[i]);
        }
    }
    return newArray;
};

/**
 * Выполняется после нахождения всех задач для отображения на странице
 * @typedef function(Task[], User, Number) utils.findTasksCallback
 * @property {Task[]} tasks задачи
 * @property {User} user - данные пользователя
 * @property {Number} pageCount - Количество страниц
 */
/**
 * Выполняется после нахождения пользователей
 * @typedef function(User[]) utils.findUsersCallback
 * @property {User[]} users пользователи
 */
/**
 * Выполняется после нахождения 1 пользователя
 * @typedef function(User) utils.findUserCallback
 * @property {User} user данные пользователя
 */
/**
 * Функция без параметров
 * @typedef function utils.voidCallback
 */
/**
 * Выполняется после нахождения всех задач
 * @typedef function(Task[]) utils.findSharedTasksCallback
 * @property {Task[]} tasks - задачи
 */
/**
 * @class
 */
function utils() {
}
/**
 * Ищет всех пользователей в БД
 * @param {utils.findUsersCallback} callback
 */
utils.prototype.findAllUsers = function (callback) {
    schemas.User.find().exec(function (err, users) {
        if (err) {
            throw err;
        }
        callback(users);
    });
};

/**
 * Удаляет пользователей с определенными именами
 * @param {User[]} userIds Имена пользователей
 * @param {utils.findUsersCallback} callback
 */
utils.prototype.deleteUsers = function (userIds, callback) {
    this.findAllUsers(function (users) {
        users = users.removeIndices(userIds, function (userRemoved) {
            userRemoved.remove();
        });
        callback(users);
    });
};

/**
 * Ищет пользователя по id
 * @param userId id пользователя
 * @param {utils.findUserCallback} callback
 */
utils.prototype.findUser = function (userId, callback) {
    schemas.User.findById(mongoose.Types.ObjectId(userId)).exec(function (err, user) {
        /* отображаем список задач */
        if (err) {
            throw err;
        }
        callback(user);
    });
};

/**
 * Ищет задачи пользователя на странице
 * @param {Number} userId id пользователя
 * @param {paginationData} paginationData Данные текущей страницы
 * @param {utils.findTasksCallback} _callback
 */
utils.prototype.findTasks = function (userId, paginationData, _callback) {
    this.findUser(userId, function (user) {
        var pageCount = 0;
        // поиск всех задач по юзеру
        var docs = schemas.Task.find()
            .populate('user')
            .find({
                    user: user
                }
                // пилим колбек, чтобы 2й раз по БД не бегать
            );
        docs.exec(function (err, _fullTasks) {
            var pageLength = +paginationData.pageLength;
            pageCount = Math.ceil(_fullTasks.length / pageLength);
            docs.skip((+paginationData.currentPage - 1) * pageLength)
                .limit(pageLength)
                .exec(function (err, _tasks) {
                    if (err) {
                        throw err;
                    }
                    _callback(_tasks, user, pageCount);
                });
        });
    });
};
/**
 * Ищем задачи, расшаренные текущему юзеру
 * @param {Number} userId id юзера
 * @param {utils.findSharedTasksCallback} callback
 */
utils.prototype.findSharedTasks = function (userId, callback) {
    this.findUser(userId, function (user) {
        schemas.Shared.find()
            .populate('shared')
            .find({
                user: user
            })
            .populate('task')
            .exec(function (err, taskIds) {
                schemas.Task.find({
                    _id: {
                        $in: taskIds
                    }
                }).exec(function (err, tasks) {
                    callback(tasks);
                });
            });
    });
};

/**
 * Расшариваем задачу
 * @param task Задача
 * @param viewer Кому расшариваем
 * @param {utils.voidCallback} callback
 */
utils.prototype.addSharedTask = function (task, viewer, callback) {
    new schemas.Shared({
        task: task,
        user: viewer
    }).save(function (err) {
        if (err) {
            throw err;
        }
    }).exec(function () {
        callback();
    });
};
/**
 * Удаляем расшаренную задачу
 * @param task Задача
 * @param viewer Кому расшариваем
 * @param {utils.voidCallback} callback
 */
utils.prototype.deleteSharedTask = function (task, viewer, callback) {
    schemas.Shared.findOneAndRemove({
        task: task,
        user: viewer
    }, {}, function () {
        callback();
    });
};
/**
 * Кому можно расшаривать задачи? (всем помимо текущего юзера)
 * @param userId id пользователя
 * @param {utils.findUsersCallback} _callback
 */
utils.prototype.findAccessibleUsers = function (userId, _callback) {
    var that = this;
    this.findAllUsers(function (users) {
        that.findUser(userId, function (user) {
            users.splice(users.indexOf(user), 1);
            _callback(users);
        })
    });
};

module.exports = new utils();
