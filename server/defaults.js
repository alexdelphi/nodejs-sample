// Дефолтные настройки
var defaultSettings = {
    paginationData: {
        // Количество задач на странице
        pageLength: 20,
        // Текущая страница
        currentPage: 1
    },
    compiledPaths: {
        pathToSearch: 'js/bin',
        runtime: 'js/src/runtime.js'
    }
};
module.exports = defaultSettings;