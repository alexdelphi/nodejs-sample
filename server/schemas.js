'use strict';
// Инициализация для app.js

function Schemas() {
    var mongoose = require('mongoose');
    var db = mongoose.createConnection('mongodb://localhost:27017/test');
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function () {
        console.log("Connected to DB");
    });
    this.db = db;
    var taskSchema = mongoose.Schema({
        name: String,
        user: [{
            type: mongoose.Schema.Types.ObjectId, ref: 'User'
        }]
    });

    this.Task = db.model('Task', taskSchema, "Tasks");
    var userSchema = mongoose.Schema({
        login: String,
        password: String,
        email: String, // plain text for now
        privileges: String
    });
    this.User = db.model('User', userSchema);

    var sharedSchema = mongoose.Schema({
        task: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Task'
        }],
        user: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }]
    });
    this.Shared = db.model('Shared', sharedSchema);
}

var instance = new Schemas();

module.exports = instance;